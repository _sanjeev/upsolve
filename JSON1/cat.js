let obj = {
	"Avengers": [

		{
		"Name" : "Tony stark",
		"also known as" : "Iron man",
		"Abilities" : [ "Genius", "Billionaire",
						"Playboy", "Philanthropist" ]
		},

		{
		"Name" : "Peter parker",
		"also known as" : "Spider man",
		"Abilities" : [ "Spider web", "Spidy sense" ]
		}
	]
}

const data = obj['Avengers'].find ((key) => {
    console.log(key);
})