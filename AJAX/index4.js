let arr = [
    {id: '1', currency: '$412', country:'india'},
    {id: '2', currency: '$10', country:'india'},
    {id: '3', currency: '$02', country:'india'},
    {id: '4', currency: '$3', country:'india'},
    {id: '5', currency: '$4', country:'india'},
    {id: '6', currency: '$2', country:'india'},
    {id: '7', currency: '$2', country:'india'},
    {id: '8', currency: '$1', country:'india'},
]

const res = arr.reduce ((acc, val) => {
    acc = acc + Number(val.currency.substr (1));
    return acc;
}, 0)

console.log(res);