const arr = [
    {name:'sanjeev', age : 21},
    {name: 'chunnu', age : 22},
    {name: 'kumar', age : 21}
]

const data = arr.reduce ((acc, val) => {
    if (!acc[val.age]) {
        acc[val.age] = [];
        acc[val.age].push (val);
    }else {
        acc[val.age].push (val);
    }
    return acc;
}, {})
