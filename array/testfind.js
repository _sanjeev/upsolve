const items = require ('./item.js');
const find = require ('./find.js');

let output = null;

const callback = ((val, target) => {
    let res = (val === target ? true : false);
    return res;
})


output = find (items, callback);
console.log(output);

let expected = true;

if (output === expected) {
    console.log('same');
}else {
    console.log('different');
}