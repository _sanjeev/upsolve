const counterFactory = () => {
    let x = 10;
    const incDec = () => {
        return {
            increment: () => {
                return x = x + 1;
            },
            decrement: () => {
                return x = x - 1;
            }
        }
    }
    return incDec();
}

module.exports = counterFactory;