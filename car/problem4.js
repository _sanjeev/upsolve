const getCarYear = ((inventory) => {
    const carYear = inventory.map ((val) => {
        return val.car_year;
    })
    return carYear;
})

module.exports = getCarYear;