const before2000 = ((val) => {
    const carYear = val.map ((data) => {
        return data.car_year;
    }).filter ((data) => {
        return data < 2000;
    })
    return carYear;
})

module.exports = before2000;