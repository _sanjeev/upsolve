const reduce = ((ele, cb, startingValue) => {
    let output;
    ele.forEach ((val) => {
        startingValue = cb (startingValue, val);
    })
    output = startingValue;
    return output;
})

module.exports = reduce;