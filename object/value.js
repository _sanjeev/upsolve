const getValue = ((obj) => {
    let output = [];
    const value = Object.entries (obj).map ((val) => {
        output.push (val[1]);
    })
    return output;
})

module.exports = getValue;