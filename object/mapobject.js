const mapObject = ((obj, cb) => {
    const res = Object.entries (obj).reduce ((acc, val) => {
        acc[val] = 'Hello' + val[1];
        return acc;
    }, {})
    cb (res);
})

module.exports = mapObject;

