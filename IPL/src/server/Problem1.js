const matches = require('./../public/output/matches.json');
const deliveries = require('./../public/output/delivery.json');
const fs = require('fs');

const getData = ((obj) => {
    const data = obj.reduce ((acc, val) => {
        if (val.toss_winner === val.winner && acc[val.winner]) {
            acc[val.winner]++;
        }else if (val.toss_winner === val.winner) {
            acc[val.winner] = 1;
        }
        return acc;
    }, {})
    fs.writeFile ('./../public/output/Problem1.json', JSON.stringify (data), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })
})

getData (matches);