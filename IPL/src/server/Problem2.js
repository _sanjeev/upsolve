const matches = require('./../public/output/matches.json');
const deliveries = require('./../public/output/delivery.json');
const fs = require('fs');

const getData = ((obj) => {
    const result = obj.reduce((acc, val) => {
        if (!acc[val.player_of_match]) {
            acc[val.player_of_match] = {};
            acc[val.player_of_match][val.season] = 1;
        }
        else {
            // if (!acc[val.player_of_match][val.season])
            //     acc[val.player_of_match][val.season] = 1;
            // else
            //     acc[val.player_of_match][val.season] += 1;
            acc[val.player_of_match][val.season] ? acc[val.player_of_match][val.season]++ : acc[val.player_of_match][val.season] = 1; 
        }
        return acc;
    }, {})
    console.log(result);
})


getData(matches);