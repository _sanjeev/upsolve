const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const getValue = require('./value.js');

const actual = getValue(testObject);
console.log(actual);

let expected = ['Bruce Wayne', 36, 'Gotham'];

if (actual.toString() === expected.toString()) {
    console.log('same');
}else {
    console.log('different');
}
