const invert = ((obj) => {
    const res = Object.entries (obj).reduce ((acc, val) => {
        acc[val[1]] = val[0];
        return acc;
    }, {}) 
    return res;
})

module.exports = invert;