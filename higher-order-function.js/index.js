import fetch from "node-fetch";
const fetchApi = (async (url) => {
    try {
        const response = await fetch (url);
        const data = await response.json();
        const povData = data.povBooks;
        console.log(povData);
        const result = povData.reduce ((accu, val) => {
            return accu + Number(val.charAt (val.length - 1));
        }, 0)
        console.log(result);
    } catch (error) {
        console.log(error);
    }
})

fetchApi ('https://anapioficeandfire.com/api/characters/583');