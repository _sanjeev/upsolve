const getData = ((url) => {
    const request = new XMLHttpRequest ();
    request.addEventListener ('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
            console.log(request.responseText);
        }else if (request.readyState === 4) {
            console.log('Not found the data');
        }
    })
    request.open ('GET', url);
    request.send();
})

getData ('https://anapioficeandfire.com/api/characters/583');