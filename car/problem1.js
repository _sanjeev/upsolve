const getId = ((obj, id) => {
    const output = obj.filter((val) => {
        return val.id === 33;
    })
    // let result = '';
    // output.forEach ((val) => {
    //     result = 'Car 33 is a ' + val.car_year + ' goes here ' + 
    //     val.car_make + 'car model ' + val.car_model;
    // })
    // return result;
    const result = output.map((val) => {
        return 'Car 33 is a ' + val.car_year + ' goes here ' +
            val.car_make + 'car model ' + val.car_model;
    })
    return result;
})

module.exports = getId;