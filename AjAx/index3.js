var array = [
    {
        firstname: 'Jack',
        lastname: 'Miller',
        age: 25,
        gender: 'm'
    }, {
        firstname: 'John',
        lastname: 'Jackson',
        age: 28,
        gender: 'm'
    }, {
        firstname: 'Jack',
        lastname: 'Evans',
        age: 21,
        gender: 'm'
    }, {
        firstname: 'Chris',
        lastname: 'Schulz',
        age: 32,
        gender: 'm'
    }];

    array.sort ((a, b) => {
        if (a.firstname < b.firstname) {
            return -1;
        }else {
            return 1;
        }
    })

console.log(array);