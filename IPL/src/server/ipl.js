const csv = require ('csvtojson');
const filePath = './../data/deliveries.csv';
const fs = require ('fs');

csv ().fromFile (filePath).then ((response) => {
    fs.writeFile ('./../public/output/delivery.json', JSON.stringify (response), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })
})