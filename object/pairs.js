const pairs = ((obj, cb) => {
    const key = Object.entries(obj).reduce ((acc, val) => {
        acc.push (val[0]);
        return acc;
    }, [])
    const value = Object.entries (obj).reduce ((acc, val) => {
        acc.push (val[1]);
        return acc;
    }, [])
    cb (key, value);
})

module.exports = pairs;