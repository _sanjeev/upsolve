const matches = require('./../public/output/matches.json');
const deliveries = require('./../public/output/delivery.json');
const fs = require('fs');

const getData = ((matches) => {

    let output = matches.reduce ((acc, val) => {
        if (val.winner === val.toss_winner) {
            if (!acc[val.winner]) {
                acc[val.winner] = 1;
            }else {
                acc[val.winner]++;
            }
        }
        return acc;
    }, {})
    // console.log(output);

    const result = matches.filter ((val) => {
        if (val.winner === val.toss_winner) {
            return true;
        }
    }).reduce ((acc, val) => {
        if (!acc[val.winner]) {
            acc[val.winner] = 1;
        }else {
            acc[val.winner] ++;
        }
        return acc;
    }, {})
    // console.log(result);

    const finalResult = Object.entries (result).sort ((a, b) => {
        return b[1] - a[1];
    })
    // console.log(finalResult);

    const out = finalResult.reduce ((acc, val) => {
        acc[val[0]] = val[1];
        return acc;
    }, {})
    console.log(out);
})

getData (matches);