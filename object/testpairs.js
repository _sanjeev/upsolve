const pairs = require('./pairs.js');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const cb = ((key, val) => {
    console.log(key);
    console.log(val);
    let expectedKey = ['name', 'age', 'location'];
    let expectedValue = ['Bruce Wayne', 36, 'Gotham'];
    if (JSON.stringify (key) === JSON.stringify (expectedKey)) {
        console.log('Key is same');
    }else {
        console.log('Key is Different');
    }

    if (JSON.stringify (val) === JSON.stringify (expectedValue)) {
        console.log('Value is same');
    }else {
        console.log('Value is different');
    }

})

pairs(testObject, cb);

