const mapObject = require('./mapobject.js');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const cb = ((val) => {
    console.log(val);
    let expected = {
        'name,Bruce Wayne': 'HelloBruce Wayne',
        'age,36': 'Hello36',
        'location,Gotham': 'HelloGotham'
    };
    if (JSON.stringify (expected) === JSON.stringify (val)) {
        console.log('same');
    }else {
        console.log('different');
    }
})

mapObject(testObject, cb);

