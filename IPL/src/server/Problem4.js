const matches = require('./../public/output/matches.json');
const deliveries = require('./../public/output/delivery.json');
const fs = require('fs');

const getData = ((matches, deliveries) => {
    // const result = deliveries.filter ((val) => {
    //     if (val.dismissal_kind != '') {
    //         return true;
    //     }
    // })
    // console.log(result);
    const result = deliveries.reduce ((acc, val) => {
        if (val.dismissal_kind != 'run out' && val.player_dismissed != '') {
            if (!acc[val.batsman]) {
                acc[val.batsman] = {};
                acc[val.batsman][val.bowler] = 1;
            }else {
                if (!acc[val.batsman][val.bowler]) {
                    acc[val.batsman][val.bowler] = 1;
                }else {
                    acc[val.batsman][val.bowler]++;
                }
            }
        } 
        return acc;
    }, {})
    // console.log(result);
    // const out = Object.entries (result).reduce ((acc, val) => {
    //     acc[val[0]] = (Object.entries(val[1]).sort ((a, b) => {
    //         return b[1] - a[1];
    //     })).slice(0,1);

    // console.log(result);

    const out = Object.entries (result).reduce ((acc, val) => {
        acc[val[0]] = (Object.entries(val[1]).sort ((a, b) => {
            return b[1] - a[1];
        }).slice (0, 1)).reduce ((accu, value) => {
            accu[value[0]] = value[1];
            return accu;            
        }, {})
        return acc;
    }, {})
     console.log(out);

    // const output = Object.entries (out).reduce ((acc, val) => {
    //     acc[val[0]] = val[1];
    //     return acc;  
    // }, {})
    // console.log(output);
})

getData (matches, deliveries);