const defaults = ((obj, defaultsprop) => {
    const res = Object.entries (obj).reduce ((acc, val) => {
        if (defaultsprop === val[0]) {
            acc[undefined] = val[1];
        }else {
            acc[val[0]] = val[1];
        }
        return acc;
    }, {})
    return res;
})

module.exports = defaults;