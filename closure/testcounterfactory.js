const counterFactory = require ('./counterfactory.js');

let res = counterFactory();

console.log(res.increment ());
console.log(res.increment ());
console.log(res.increment ());
console.log(res.increment ());
console.log(res.decrement());
console.log(res.decrement());