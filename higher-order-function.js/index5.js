let data = [
    ["The", "red", "horse"],
    ["Plane", "over", "the", "ocean"],
    ["Chocolate", "ice", "cream", "is", "awesome"],
    ["this", "is", "a", "long", "sentence"]
];

const newData = data.map ((val) => {
    return val.reduce ((acc, value) => {
        console.log(acc);
        acc.push (value);
        return acc;
    },[])
})

console.log(newData);