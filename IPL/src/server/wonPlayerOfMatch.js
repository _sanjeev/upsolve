const matches = require('./../public/output/matches.json');
const deliveries = require('./../public/output/delivery.json');

const getData = ((matches, deliveries) => {
    const out = matches.reduce ((acc, val) => {
        if (!acc[val.player_of_match]) {
            acc[val.player_of_match] = {};
            acc[val.player_of_match][val.season] = 1;
        }else {
            if (!acc[val.player_of_match][val.season]) {
                acc[val.player_of_match][val.season] = 1;
            }else {
                acc[val.player_of_match][val.season]++;
            }
        }
        return acc;
    }, {})

    console.log(out); 

})

getData (matches, deliveries);