const filter = ((ele, cb) => {
    let output = [];
    ele.forEach ((val) => {
        if (cb (val))
        {
            output.push (val);
        }
    })
    return output;
})

module.exports = filter;