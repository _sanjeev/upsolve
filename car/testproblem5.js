const inventory = require('./data.js');
const before2000 = require('./problem5.js');

const actual = before2000(inventory);
console.log(actual);
console.log('Older car after 2000', inventory.length - actual.length);
const expected = [
    1983, 1990, 1995, 1987, 1996,
    1997, 1999, 1987, 1995, 1994,
    1985, 1997, 1992, 1993, 1964,
    1999, 1991, 1997, 1992, 1998,
    1965, 1996, 1995, 1996, 1999
];
if (actual.toString() === expected.toString()) {
    console.log('same');
}else {
    console.log('different');
}
