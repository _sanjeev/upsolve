const items = require ('./item.js');
const filter = require ('./filter.js');

const cb = (val) => {
    if (val % 2 === 0) {
        return val;
    }else {
        return 0;
    }
}

let actual = filter (items, cb);
console.log(actual);

let expected = [2,4];

if (actual.toString () === expected.toString ()) {
    console.log('same');
}else {
    console.log('different');
}
