const matches = require('./../public/output/matches.json');
const deliveries = require('./../public/output/delivery.json');
const fs = require('fs');

const getData = ((matches, deliveries) => {
    const result = deliveries.filter ((val) => {
        if (val.is_super_over != '0') {
            return true;
        }
    })
    const out = result.reduce ((accu, val) => {
        if (!accu[val.bowler]) {
            accu[val.bowler] = [];
            accu[val.bowler][0] = 1;
            accu[val.bowler][1] = Number(val.total_runs);
        }else {
            accu[val.bowler][0] += 1;
            accu[val.bowler][1] += Number(val.total_runs);
        }
        return accu;
    }, {})
    // console.log(out);
    const res = Object.fromEntries(Object.entries (out).map ((key) => {
        let over = Math.floor(key[1][0] / 6); 
        let ball = (key[1][0] % 6) * 0.1;
        let totalOver = over + ball;
        key[1] = (key[1][1] / totalOver).toFixed(2);
        return key;
    }).sort ((a, b) => {
        return a[1] - b[1];
    }).slice (0, 1));
    console.log(res); 
})

getData (matches, deliveries);
