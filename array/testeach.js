const each = require ('./each.js');
const items = require ('./item.js');
let output = [];
const callback = (val) => {
    output.push (val);
}
let expected = items;
each (items, callback); 
console.log(output);

if (output.toString () === expected.toString()) {
    console.log('same');
}else {
    console.log('different');
}