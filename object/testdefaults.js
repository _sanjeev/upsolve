const defaults = require('./defaults.js');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const actual = defaults(testObject, 'name');
console.log(actual);
const expected = { undefined: 'Bruce Wayne', age: 36, location: 'Gotham' };

if (JSON.stringify (actual) === JSON.stringify (expected)) {
    console.log('same');
}else {
    console.log('different');
}