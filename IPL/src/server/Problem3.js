const matches = require('./../public/output/matches.json');
const deliveries = require('./../public/output/delivery.json');
const fs = require('fs');

const getData = ((deliveries, matches) => {

    const result = deliveries.map((key) => {
        const out = matches.find((val) => {
            if (val.id === key.match_id) {
                return true;
            }
        })
        key['season'] = out.season;
        return key;
    })

    const output = result.reduce((acc, val) => {
        // if (!acc[val.batsman]) {
        //     acc[val.batsman] = {};
        //     acc[val.batsman][val.season] = 1;
        // }else {
        //     if (acc[val.batsman][val.season]) {
        //         acc[val.batsman][val.season] += 1;
        //     }else {
        //         acc[val.batsman][val.season] = 1;
        //     }
        // }
        if (!acc[val.batsman]) {
            acc[val.batsman] = {};
            acc[val.batsman][val.season] = [];
            acc[val.batsman][val.season][0] = 1;
            acc[val.batsman][val.season][1] = Number(val.total_runs);
        } else {
            if (!acc[val.batsman][val.season]) {
                acc[val.batsman][val.season] = [];
                acc[val.batsman][val.season][0] = 1;
                acc[val.batsman][val.season][1] = Number(val.total_runs);
            } else {
                acc[val.batsman][val.season][0] +=1;
                acc[val.batsman][val.season][1] += Number(val.total_runs);
            }
        }
        return acc;
    }, {})

    const res = Object.entries(output).reduce ((acc, val) => {
        acc[val[0]]= Object.entries (val[1]).reduce ((accu, curr) => {
            accu[curr[0]] = ((curr[1][1] / curr[1][0]) * 100).toFixed(2);
            return accu;
        }, {})
        return acc;
    }, {})

    console.log(res);
})

getData(deliveries, matches);