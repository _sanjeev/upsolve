const getData = ((url) => {
    return new Promise((resolve, reject) => {
        fetch(url).then((data) => {
            return data.json();
        }).then((data) => {
            const newData = data['tvSeries'];
            const ans = newData.reduce ((acc, val) => {
                acc = acc + Number (val.charAt (val.length - 1));
                return acc;
            }, 0)
            console.log(ans);
        }).catch((err) => {
            reject(err);
        })
})
})

getData('https://anapioficeandfire.com/api/characters/583').then((data) => {
    console.log(data);
}).catch((err) => {
    console.log(err);
});