const matches = require('./../public/output/matches.json');
const deliveries = require('./../public/output/delivery.json');
const fs = require('fs');

const top10Bowlers = ((matches, deliveries) => {
    const teamData2015 = matches.filter((key) => {
        return key.season === '2015';
    })
    const allData2015 = deliveries.filter((key) => {
        return key.match_id >= teamData2015[0].id && key.match_id <= teamData2015[teamData2015.length - 1].id;
    })
    const output = allData2015.reduce((acc, val) => {
        if (acc[val.bowler]) {
            acc[val.bowler] += ((Number(val.total_runs) / ((Number(val.over)) + Math.floor (Number(val.ball) / 6) + Number (val.ball) % 6)));
        } else {
            acc[val.bowler] = ((Number(val.total_runs) / ((Number(val.over)) + Math.floor (Number(val.ball) / 6) + Number (val.ball) % 6)));
        }
        return acc;
    }, {});
    // Object.entries(output).sort((a, b) => {
    //     return b[1] - a[1];
    // })
    const sortable = Object.fromEntries(
        Object.entries(output).sort(([, a], [, b]) => b - a).slice (0, 10)
    );
    console.log(sortable);
    // const out = Object.entries (sortable).reduce ((acc, val, index) => {
    //     if (acc.length < 11) {
    //         acc.push (val[0] + ', ' + val[1]);
    //     }
    //     return acc;
    // }, [])
    // console.log(out);

    fs.writeFile ('./../public/output/problem4.json', JSON.stringify (sortable), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })

})

top10Bowlers(matches, deliveries);