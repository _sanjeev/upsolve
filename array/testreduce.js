const reduce = require("./reduce");
const items = require ('./item.js');
let actual;
const cb = (svalue, value) => {
    return svalue + value;
}
actual = reduce (items, cb, 0);
console.log(actual);

let expected = 20;
if (actual === expected) {
    console.log('same');
}else {
    console.log('different');
}