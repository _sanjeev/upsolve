const matches = require('./../public/output/matches.json');
const fs = require ('fs');

const data = ((obj) => {
    // const res = obj.reduce ((accu, val) => {
    //     if (accu[val.winner]) {
    //         accu[val.winner]++;
    //     }else {
    //         accu[val.winner] = 1;
    //     }
    //     return accu;
    // }, {})
    // console.log(res);
    // fs.writeFile ('./../public/output/problem2.json', JSON.stringify (res), 'utf-8', (err) => {
    //     if (err) {
    //         console.log(err);
    //     }
    // })

    const output = obj.reduce ((acc, val) => {
        if (!acc[val.winner]) {
            acc[val.winner] = {};
            acc[val.winner][val.season] = 1;
        }else {
            if (!acc[val.winner][val.season]) {
                acc[val.winner][val.season] = 1;
            }else {
                acc[val.winner][val.season]++;
            }
        }
        return acc;
    }, {})
    console.log(output);
})

data (matches);