const getSortedCar = ((inventory) => {
    const carModel = inventory.map ((val) => {
        return val.car_model;
    })
    
    for (let i = 0; i < carModel.length - 1; i++) {
        for (let j = i + 1; j < carModel.length; j++) {
            if (carModel[j] < carModel[i]) {
                let temp = carModel[i];
                carModel[i] = carModel[j];
                carModel[j] = temp;
            }
        }
    }
    return carModel;

})

module.exports = getSortedCar;