const matches = require('./../public/output/matches.json');
const fs = require ('fs');

const problem1 = ((obj) => {
    const res = obj.reduce ((acc, val) => {
        if (acc[val.season]) {
            acc[val.season]++;
        }else {
            acc[val.season] = 1;
        }
        return acc;
    }, {})
    fs.writeFile ('./../public/output/problem1.json', JSON.stringify (res), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })
})

problem1 (matches);
