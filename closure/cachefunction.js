const cache = ((cb) => {
    let obj = {};
    const newCahche = ((n) => {
        if (obj[n]) {
            return obj[n];
        }else {
            let result = cb (n);
            obj[n] = result;
            return obj[n];
        }
    })
    return {newCahche};
})

module.exports = cache;