const selectedCars = ((val) => {
    const car = val.filter ((data) => {
        return (data.car_make === 'Audi' || data.car_make === 'BMW');
    })
    return car;
})

module.exports = selectedCars;