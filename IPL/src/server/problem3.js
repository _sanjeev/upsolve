const matches = require('./../public/output/matches.json');
const deliveries = require('./../public/output/delivery.json');
const fs = require ('fs');

const extraRun = ((matches) => {
    const temp2016 = matches.filter ((val) => {
        return val.season === "2016";
    })
    console.log(temp2016[0]);
    const teamData2016 = deliveries.filter ((key) => {
        return key.match_id >= temp2016[0].id && key.match_id <= temp2016[temp2016.length - 1].id;
    })
    // console.log(teamData2016);
    const result = teamData2016.reduce ((acc, val) => {
        if (acc [val.bowling_team]) {
            acc[val.bowling_team] += Number(val.extra_runs);
        }else {
            acc[val.bowling_team] = Number(val.extra_runs);
        }
        return acc;
    }, {})
    console.log(result);
    fs.writeFile ('./../public/output/problem3.json', JSON.stringify (result), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })
})

extraRun (matches);
