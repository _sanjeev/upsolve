const getdata = ((url) => {
    return new Promise ((resolve, reject) => {
        fetch (url).then ((data) => {
            return data.json ();
        }).then ((data) => {
            resolve (data);
        }).catch ((err) => {
            reject ('Not found the data');
        })
    })
})

getdata ('https://anapioficeandfire.com/api/characters/583').then ((data) => {
    console.log(data);
}).catch ((err) => {
    console.log(err);
});