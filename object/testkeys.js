const key = require('./keys.js');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const actual = key(testObject);
console.log(actual);
const expected = ['name', 'age', 'location'];

if (actual.toString() === expected.toString()) {
    console.log('same');
}else {
    console.log('different');
}