const each = ((ele, cb) => {
    for (let index = 0; index < ele.length; index++) {
        cb (ele[index]);
    }
})

module.exports = each;