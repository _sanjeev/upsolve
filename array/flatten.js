const flatten = (ele) => {
    let output = [];
    ele.forEach ((val) => {
        if (Array.isArray (val)) {
            output = output.concat (flatten (val));
        }else {
            output.push (val);
        }
    })
    return output;
}

module.exports = flatten;